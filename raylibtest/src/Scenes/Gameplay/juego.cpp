#include "raylib.h"
#include "juego.h"

namespace endless
{
	bool gameover = false;
	namespace game
	{
		const int lineMax = 3;
		struct enemy
		{
			Rectangle colliderEnemy;
		}enemy;
		struct player
		{
			Rectangle colliderPlayer;

		}player1;
		struct line
		{
			Rectangle line;
		}line[lineMax];
		void init( )
		{


			// Initialization
			//--------------------------------------------------------------------------------------
			const int screenWidth = 800;
			const int screenHeight = 500;
			int score = 0;
			const int heightLine = 10;
			const int widthLine = 800;

			player1.colliderPlayer.x = 100;
			player1.colliderPlayer.y = 210;
			player1.colliderPlayer.height = 40;
			player1.colliderPlayer.width = 40;
			for ( short i = 0; i < lineMax; i++ )
			{
				line[i].line.x = 0;
				line[i].line.height = 10;
				line[i].line.width = 800;
				line[i].line.y = 100 + ( i * 150 );
			}

			enemy.colliderEnemy.x = 780;
			enemy.colliderEnemy.height = 40;
			enemy.colliderEnemy.width = 40;

			switch ( GetRandomValue(0, 2) )
			{
			case 0:

				enemy.colliderEnemy.y = 60;


				break;
			case 1:

				enemy.colliderEnemy.y = 210;


				break;
			case 2:

				enemy.colliderEnemy.y = 360;


				break;
			default:
				break;

			}


			InitWindow(screenWidth, screenHeight, "");

			SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
			//--------------------------------------------------------------------------------------

			// Main game loop
		}

		void update( )
		{
			if ( player1.colliderPlayer.y > 150 )
			{
				if ( IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP) ) player1.colliderPlayer.y -= 150;
			}
			if ( player1.colliderPlayer.y < 250 )
			{
				if ( IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN) ) player1.colliderPlayer.y += 150;
			}
			enemy.colliderEnemy.x -= GetRandomValue(200, 500) * GetFrameTime();
			if ( CheckCollisionRecs(player1.colliderPlayer,enemy.colliderEnemy) )
			{
				gameover = true;
			}
		}
		void draw( )
		{
			for ( size_t i = 0; i < lineMax; i++ )
			{
				DrawRectangleRec(line[i].line, RED);
			}
			if ( gameover )
			{
				DrawText("loss", 0 +50, 50, 29, BLACK);
			}

			DrawRectangleRec(player1.colliderPlayer, BLACK);
			DrawRectangleRec(enemy.colliderEnemy, BLUE);




			//DrawText(TextFormat("SCORE %4i", score), 120, 20, 20, LIGHTGRAY);


			//----------------------------------------------------------------------------------

		}
		void deinit( )
		{

			CloseWindow( );        // Close window and OpenGL context
			//--------------------------------------------------------------------------------------

		}
		
	}
}
