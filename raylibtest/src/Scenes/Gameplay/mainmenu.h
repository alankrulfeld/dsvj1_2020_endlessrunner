#pragma once


namespace endless
{
	enum class Scenes
	{
		mainMenu = 1, gameplay, instructions, exit
	};
	extern Scenes scene;
	namespace mainmenu
	{
		void init( );
		void update( );
		void draw( );
		void deinit( );
	}
}




