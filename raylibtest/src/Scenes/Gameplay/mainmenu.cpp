#include "raylib.h"
#include "mainmenu.h"
#include "../../Run/run.h"
namespace endless
{
	const int amountMenu = 3;
	struct Button
	{
		Rectangle rec;

	};
	Button button[amountMenu];
	namespace mainmenu
	{
		void init( )
		{
			for ( short i = 0; i < amountMenu; i++ )
			{
				button[i].rec.width = 160;

				button[i].rec.x = 400- button[i].rec.width/2;
				button[i].rec.height = 60;
				button[i].rec.y = ( i + 1 ) * 100;
			}
		}
		void update( )
		{
			for ( int i = 0; i < ( amountMenu ); i++ )
			{
				if ( CheckCollisionPointRec(GetMousePosition( ), button[i].rec) )
				{
					if ( IsMouseButtonPressed(MOUSE_LEFT_BUTTON) )
					{
						switch ( i )
						{
						case 0:
							scene = Scenes::gameplay;
							break;

						case 1:
							scene = Scenes::instructions;

							break;

						case 2:
							endGame = true;

							break;



						default:

							break;
						}
					}
				}
			}
		}
		void draw( )
		{
			for ( short i = 0; i < amountMenu; i++ )
			{
				DrawRectangleRec(button[i].rec, BLACK);

			}
			DrawText("endless runner alpha - by Alan Krulfeld", 0 + button[0].rec.width / 2, 50, 29, BLACK);
			DrawText("jugar", button[0].rec.x+button[0].rec.width/2-10, button[0].rec.y + button[0].rec.height/2, 20, WHITE);
			DrawText("instrucciones", button[1].rec.x +10, button[1].rec.y + button[1].rec.height/2, 20, WHITE);
			DrawText("salir", button[2].rec.x+ button[2].rec.width/2-10, button[2].rec.y + button[2].rec.height / 2, 20, WHITE);

		}
		void deinit( )
		{

		}
	}
}
