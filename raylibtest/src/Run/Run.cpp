#include "../Scenes/Gameplay/juego.h"
#include "../Scenes/Gameplay/instructions.h"
#include "../Scenes/Gameplay/mainmenu.h"
#include "../Scenes/Gameplay/juego.h"

#include "raylib.h"

	namespace endless
	{
		bool endGame = false;


		Scenes scene = Scenes::mainMenu;

		

		void init( )
		{
			const int screenWidth = 800;
			const int screenHeight = 500;
			InitWindow(screenWidth, screenHeight, "endless runner alpha - by Alan Krulfeld");
			SetTargetFPS(60);
			SetExitKey(KEY_F4);

			game::init( );
			mainmenu::init( );



		}

		void update( ) {

			switch ( scene )
			{
			case Scenes::mainMenu:
				mainmenu::update( );
				break;

			case Scenes::gameplay:
				game::update( );
				break;

				/*	case instructions:
						instructions::update( );
						break;*/

			default:
				break;
			}
		}

		void draw( ) 
		{

			BeginDrawing( );

			ClearBackground(WHITE);

			switch ( scene )
			{
			case Scenes::mainMenu:
				mainmenu::draw( );
				break;

			case Scenes::gameplay:
				game::draw( );
				break;

				//case instructions:
				//	instructions::draw( );
				//	break;

			default:
				break;
			}

			EndDrawing( );
		}

		void deinit( ) 
		{

			mainmenu::deinit( );
			game::deinit( );
			CloseWindow( );
		}
		void runGame( )
		{

			init( );

			while ( !WindowShouldClose( ) && !endGame )
			{
				update( );
				draw( );
			}

			deinit( );
		}
	}

