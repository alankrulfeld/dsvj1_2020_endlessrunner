#pragma once
#include "raylib.h"

struct Ball
{
	Vector2 position;
	Vector2 speed;
	int radius;
	bool active;
	bool sideColision;
} ball;
ball.position.x = 400;
ball.position.y = 250;
ball.radius = 10;
ball.speed.x = 0;
ball.speed.y = 0;
ball.sideColision = false;
Rectangle player1;
player1.x = 20;
player1.y = 200;
player1.height = 80;
player1.width = 20;
Rectangle player2;
player2.x = 760;
player2.y = 200;
player2.height = 80;
player2.width = 20;
