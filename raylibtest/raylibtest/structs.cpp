#include "raylib.h"

struct Ball
{
	Vector2 position;
	Vector2 speed;
	int radius;
	bool active;
	bool sideColision;
} ball;
