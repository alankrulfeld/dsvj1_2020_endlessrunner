#include "raylib.h"
namespace juego
{
	void juego( )
	{
		// Initialization
		//--------------------------------------------------------------------------------------
		const int screenWidth = 800;
		const int screenHeight = 500;
		int score = 0;
		const int heightLine = 10;
		const int widthLine = 800;
		Rectangle player;
		player.x = 100;
		player.y = 210;
		player.height = 40;
		player.width = 40;
		Rectangle line1;
		line1.x = 0;
		line1.y = 100;
		line1.height = 10;
		line1.width = 800;
		Rectangle line2;
		line2.x = 0;
		line2.y = 250;
		line2.height = 10;
		line2.width = 800;
		Rectangle line3;
		line3.x = 0;
		line3.y = 400;
		line3.height = 10;
		line3.width = 800;

		InitWindow(screenWidth, screenHeight, "");

		SetTargetFPS(60);               // Set our game to run at 60 frames-per-second
		//--------------------------------------------------------------------------------------

		// Main game loop
		while ( !WindowShouldClose( ) )    // Detect window close button or ESC key
		{


			int timer = GetTime( );

			// Update
			//----------------------------------------------------------------------------------

			if ( player.y > 150 )
			{
				if ( IsKeyPressed(KEY_W) || IsKeyPressed(KEY_UP) ) player.y -= 150;
			}
			if ( player.y < 250 )
			{
				if ( IsKeyPressed(KEY_S) || IsKeyPressed(KEY_DOWN) ) player.y += 150;
			}



			// TODO: Update your variables here
			//----------------------------------------------------------------------------------

			// Draw
			//----------------------------------------------------------------------------------
			BeginDrawing( );

			DrawRectangleRec(line1, RED);
			DrawRectangleRec(line2, RED);
			DrawRectangleRec(line3, RED);

			DrawRectangleRec(player, BLACK);



			ClearBackground(WHITE);

			//DrawText(TextFormat("SCORE %4i", score), 120, 20, 20, LIGHTGRAY);


			EndDrawing( );
			//----------------------------------------------------------------------------------

		}

		// De-Initialization
		//--------------------------------------------------------------------------------------
		CloseWindow( );        // Close window and OpenGL context
		//--------------------------------------------------------------------------------------

}
